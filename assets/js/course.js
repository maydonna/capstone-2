//window.location.search returns the query string part of the URL
// console.log(window.location.search)

//instantiate a URLSearchParams object so we can execute methods to access parts of the query string
let params = new URLSearchParams(window.location.search)

let courseId = params.get('courseId')

//retrieve the JWT stored in our local storage
let token = localStorage.getItem("token");
//retrieve the userId stored in our local storage
let userId = localStorage.getItem("id");
let adminUser = localStorage.getItem("isAdmin")

let courseName = document.querySelector("#courseName");
let courseDesc = document.querySelector("#courseDesc");
let coursePrice = document.querySelector("#coursePrice");
let enrollContainer = document.querySelector("#enrollContainer");
let yyy = document.querySelector("#enrolleesNames");

fetch(`https://donna-capstone2.herokuapp.com/api/courses/${courseId}`)
.then((res) => res.json())
.then((data) => {

	let userIds = []
let userFirstNames = []
let userLastNames = []

	for(let i = 0; i < data.enrollees.length; i++){
		// for(let i = 0; i < data.course.length; i++){
		userIds.push(data.enrollees[i].userId)
		// courseNames.push(data.course[i].name)
		// console.log(data.enrollments.length)
		console.log(data.enrollees[i].userId)

		 // a = data.enrollments[i].courseId;
		// console.log(data.course[i].name)
	}
	
	courseName.innerHTML = data.name
	courseDesc.innerHTML = data.description
	coursePrice.innerHTML = data.price

	fetch('https://donna-capstone2.herokuapp.com/api/users')
	.then(res => res.json())
	.then(data => {

		for(let i = 0; i < data.length; i++){

			if(userIds.includes(data[i]._id)){
				
				userFirstNames.push(data[i].firstName);
				
			}
			if(userIds.includes(data[i]._id)){
				
				userLastNames.push(data[i].lastName);
				
			}

			}
			
			let usersList = userFirstNames.join("\n")
			console.log(usersList)
			let usersList1 = userLastNames.join("\n")
			console.log(usersList1)

			let student = usersList.usersList1
			console.log(student)


	
	if(adminUser == "false" || !adminUser){
		yyy.innerHTML = null
	}else{
		yyy.innerHTML = `<p class="font-weight-bolder">Enrollees: </p>First Names: ${userFirstNames}</p>
		<p>Last Names: ${userLastNames}</p>`
	}

	})
	
	if(adminUser == "false" || !adminUser){
		enrollContainer.innerHTML = `<button id="enrollButton" class="btn btn-block btn-info">Enroll</button>`
	}else{
		enrollContainer.innerHTML = null
	}

	let enrollButton = document.querySelector("#enrollButton")

	enrollButton.addEventListener("click", () => {
		//enroll the user for the course
		fetch('https://donna-capstone2.herokuapp.com/api/users/enroll', {
			method: "PUT",
			headers: {
				'Content-Type': 'application/json',
				'Authorization': `Bearer ${token}`
			},
			body: JSON.stringify({
				courseId: courseId,
				userId: userId
			})
		})
		.then(res => {
			return res.json()
		})
		.then(data => {
			if(data === true){
				//enrollment is infoful
				alert("Thank you for enrolling! See you!")
				window.location.replace('./courses.html')
			}else{
				alert("Enrollment failed")
			}
		})
	})

})