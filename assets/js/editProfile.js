// instantiate a URLSearchParams object so we can execute methods to access 
// specific parts of the query string
let params = new URLSearchParams(window.location.search);

// get method returns the value of the key passed in as an argument

let userId = params.get('userId')

let firstname = document.querySelector("#firstName")
let lastName = document.querySelector("#lastName")
let mobileNo = document.querySelector("#mobileNumber")
let email = document.querySelector("#userEmail")

fetch(`https://donna-capstone2.herokuapp.com/users/${userId}`)
.then(res => {
    return res.json()
})
.then(data => {
	// assign the current values as placeholders
	firstname.placeholder = data.firstname
	lastName.placeholder = data.lastName
	mobileNo.placeholder = data.mobileNo
	email.placeholder = data.email

	document.querySelector("#profileContainer").addEventListener("submit", (e) => {
		e.preventDefault()

		let firstName = firstname.value
		let lastName = lastName.value
		let mobileNo = mobileNo.value
		let email = email.value

		let token = localStorage.getItem('token')

		fetch('https://donna-capstone2.herokuapp.com/api/users', {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`
            },
            body: JSON.stringify({
                userId: userId,
                firstname: firstName,
                lastName: lastName,
                mobileNo: mobileNo,
 				email: email
            })
        })
        .then(res => {
            return res.json()
        })
        .then(data => {
            //creation of new course infoful
            if(data === true){
                //redirect to courses index page
                window.location.replace("./profiles.html")
            }else{
                //error in creating course, redirect to error page
                alert("Something went wrong")
            }
        })
	})
})