
let params = new URLSearchParams(window.location.search);
let courseId = params.get('courseId')

let token = localStorage.getItem("token")


fetch(`https://donna-capstone2.herokuapp.com/api/courses/activate/${courseId}`, {
            method: 'DELETE',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`
            },
        })
        .then(res => {
            return res.json()
        })
        .then(data => {
            console.log("activating")
            //creation of new course successful
            if(data === true){
                alert("successfully activated")
                console.log(isActive)
                //redirect to courses index page
                window.location.replace("./courses.html")
            }else{
                //error in creating course, redirect to error page
                alert("Something went wrong")
            }
        })