let adminUser = localStorage.getItem("isAdmin")
let modalButton = document.querySelector('#adminButton')
let cardFooter;

if(adminUser == "false" || !adminUser){
	modalButton.innerHTML = null
}else{
	modalButton.innerHTML = `<div class="col-md-2 offset-md-10">
	<a href="./addCourse.html" class="btn btn-block btn-info">Add Course</a>
</div>`
}

//fetch the courses from our API
fetch('https://donna-capstone2.herokuapp.com/api/users')
.then(res => res.json())
.then(data => {
	// console.log(data)
	let userData;

	//if the number of courses fetched is less than 1, display no courses available.
	if(data.length < 1){
		userData = "No courses available."
	}else{
		//else iterate the courses collection and display each course
		userData = data.map(user => {
			// console.log(course._id)
			if(adminUser == "false" || !adminUser){
				//check if the user is not an admin
				//if not an admin, display the select course button
				cardFooter = `<a href="./profile.html?userId=${user._id}" value=${user._id} class="btn btn-info text-white btn-block editButton">Select Course</a>`
			}else{
				//if user is an admin, display the edit course button
				cardFooter = `<a href="./profile.html?userId=${user._id}" value=${user._id} class="btn btn-info text-white btn-block editButton">Edit</a>

<a href="./deleteUser.html?userId=${user._id}" value=${user._id} class="btn btn-info text-white btn-block editButton">Delete</a>`
			}

			return (
					`<div class="col-md-6 my-3">
	<div class="card">
		<div class="card-body">
			<h5 class="card-title">${user.firstName}</h5>
			<p class="card-text text-left">${user.lastName}</p>
			<p class="card-text text-right">₱ ${user.email}</p>
		</div>

		<div class="card-footer">
			${cardFooter}
		</div>
	</div>
</div>`
					)

		}).join("")
		//since the collection is an array, we can use the join method to indicate the separator of each element. We replaced the commas with an empty strings to remove them.
	}

	let containera = document.querySelector('#usersContainer')
	containera.innerHTML = userData
})