
let adminUser = localStorage.getItem("isAdmin")
let modalButton = document.querySelector('#adminButton')
let modalButton2 = document.querySelector('#adminButton2')
let modalButton3 = document.querySelector('#adminButton3')
let viewAllArchiveButton = document.querySelector('#adminButtonArchive')
let viewAllCoursesButton = document.querySelector('#adminButtonAllCourses')
let cardFooter;

if(adminUser == "false" || !adminUser){
	modalButton.innerHTML = null
}else{
		modalButton.innerHTML = `<div class="col-md-2 offset-md-10">
			<a href="./addCourse.html" class="btn btn-block btn-info">Add Course</a>
		</div>`
		modalButton2.innerHTML = `<div class="col-md-2 offset-md-10">
			<a href="./allCourses.html" class="btn btn-block btn-info">All Courses</a>
		</div>`
		modalButton3.innerHTML = `<div class="col-md-2 offset-md-10">
			<a href="./archived.html" class="btn btn-block btn-info">Archives</a>
		</div>`
		
}

//fetch the courses from our API
fetch('https://donna-capstone2.herokuapp.com/api/courses')
.then(res => res.json())
.then(data => {
	// console.log(data)
	let courseData;
	
	

	//if the number of courses fetched is less than 1, display no courses available.
	if(data.length < 1){
		courseData = "No courses available."
	}else{
		//else iterate the courses collection and display each course
		courseData = data.map(course => {
			// console.log(course._id)
			if(adminUser == "false" || !adminUser){
				//check if the user is not an admin
				//if not an admin, display the select course button
				cardFooter = `<a href="./course.html?courseId=${course._id}" value=${course._id} class="btn btn-info text-white btn-block selectButton">Select Course</a>`
			}else{
				//if user is an admin, display the edit course button
				cardFooter = `<a href="./editCourse.html?courseId=${course._id}" value=${course._id} class="btn btn-info text-white btn-block editButton">Edit</a>
							<a href="./deleteCourse.html?courseId=${course._id}" value=${course._id} id="deleteCourse" class="btn btn-info text-white btn-block deleteButton">Delete</a>
							<a href="./course.html?courseId=${course._id}" value=${course._id} id="viewCourse" class="btn btn-info text-white btn-block viewButton">View</a>`
			}
			return (
						`<div class="col-md-6 my-3">
							<div class="card">
								<div class="card-body">
									<h5 class="card-title font-weight-bolder">${course.name}</h5>
										<p class="card-text text-left">${course.description}</p>
										<p class="card-text text-right">₱ ${course.price}</p>
								</div>
								<div class="card-footer">
									${cardFooter}
								</div>
							</div>
						</div>`
					)

		}).join("")
		//since the collection is an array, we can use the join method to indicate the separator of each element. We replaced the commas with an empty strings to remove them.
	}

	let container = document.querySelector('#coursesContainer')
	container.innerHTML = courseData


})